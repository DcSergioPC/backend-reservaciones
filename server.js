
/*
// server.js
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const db = require('./app/models');

// Sincronización de la base de datos
db.sequelize.sync();

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

// Ruta de prueba
app.get('/', (req, res) => {
    res.json({ message: 'Bienvenido Node backend 2020' });
});

// Importación de rutas específicas
const tableRoutes = require('./app/routes/table.routes');
const clientRoutes = require('./app/routes/client.routes');
const restaurantRoutes = require('./app/routes/restaurant.routes'); // Asegúrate de importar las rutas de restaurantes

// Uso de las rutas
app.use('/api/tables', tableRoutes);
app.use('/api/clients', clientRoutes);
app.use('/api/restaurants', restaurantRoutes); // Asegúrate de usar las rutas de restaurantes bajo /api/restaurants

// Puerto de escucha
const PORT = process.env.PORT || 9090;
app.listen(PORT, () => {
    console.log(`Servidor corriendo en puerto ${PORT}.`);
});
*/

// server.js
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const db = require('./app/models');

// Sincronización de la base de datos
db.sequelize.sync();

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

// Ruta de prueba
app.get('/', (req, res) => {
    res.json({ message: 'Bienvenido Node backend 2020' });
});

// Importación de rutas específicas
const tableRoutes = require('./app/routes/table.routes');
const clientRoutes = require('./app/routes/client.routes'); // Asegúrate de importar las rutas de clientes
const restaurantRoutes = require('./app/routes/restaurant.routes');
const reservationroutes = require('./app/routes/reservation.routes');
const categoriaRoutes = require('./app/routes/categoria.routes');
const productoRoutes = require('./app/routes/producto.routes');
const consumoRoutes = require('./app/routes/consumo.routes');

// Uso de las rutas
app.use('/api/tables', tableRoutes);
app.use('/api/clients', clientRoutes); // Asegúrate de usar las rutas de clientes bajo /api/clients
app.use('/api/restaurants', restaurantRoutes);
app.use('/api/reservations', reservationroutes);
app.use('/api/categoria', categoriaRoutes);
app.use('/api/producto', productoRoutes);
app.use('/api/consumo', consumoRoutes);

// Puerto de escucha
const PORT = process.env.PORT || 9090;
app.listen(PORT, () => {
    console.log(`Servidor corriendo en puerto ${PORT}.`);
});

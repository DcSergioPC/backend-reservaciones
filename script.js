document.addEventListener('DOMContentLoaded', function() {

    const modal = document.getElementById("myModal");
    const closeModal = document.getElementsByClassName("close")[0];
    const createModal = document.getElementById("createModal");
    const openCreateModalBtn = document.getElementById("openCreateModalBtn");
    const closeCreateModal = document.getElementById("closeCreateModal");
        
    let productos = [];
    let clientes = [];
    let consumo = {}
    let newConsumo = {
        "id": "2",
        "clientId": "",
        "estado": "abierto",
        "total": 0,
        "fecha_consumo": new Date().toISOString(),
        "fecha_cierre": null,
        "cliente": {},
        "detalle": []
    };
    let tableId = null;
    fetch('http://localhost:9090/api/producto')
        .then(response => response.json())
        .then(data => {
            productos = data;
        })
        .catch(error => console.error(error));

    fetch('http://localhost:9090/api/clients')
        .then(response => response.json())
        .then(data => {
            clientes = data;
        })
        .catch(error => console.error(error));

        closeCreateModal.onclick = function() {
            createModal.style.display = "none";
        }
        // Función para llenar la lista de clientes
        // Función para llenar la lista de clientes
        function populateClientes() {
            const clientesSelect = document.getElementById("clientes");
            const clientesFilter = document.getElementById("clientesFilter");
            clientesSelect.innerHTML = "";

            // Llenar la lista de clientes inicialmente
            clientes.forEach(cliente => {
                const option = document.createElement("option");
                option.value = cliente.id;
                option.text = `${cliente.firstName} ${cliente.lastName}`;
                clientesSelect.appendChild(option);
            });

            clientesSelect.value = consumo.clientId; // Seleccionar el cliente actual

            clientesSelect.onchange = function() {
                const selectedCliente = clientes.find(cliente => cliente.id === clientesSelect.value);
                consumo.cliente = selectedCliente;
                fetch(`http://localhost:9090/api/consumo/client/${consumo.id}/${selectedCliente.id}`)
                populateModal(consumo);
            };

            // Filtrar clientes cuando se escribe en el campo de búsqueda
            clientesFilter.oninput = function() {
                const filterValue = clientesFilter.value.toLowerCase();
                clientesSelect.innerHTML = "";

                const filteredClientes = clientes.filter(cliente => {
                    const fullName = `${cliente.firstName} ${cliente.lastName}`.toLowerCase();
                    return fullName.includes(filterValue);
                });

                filteredClientes.forEach(cliente => {
                    const option = document.createElement("option");
                    option.value = cliente.id;
                    option.text = `${cliente.firstName} ${cliente.lastName}`;
                    clientesSelect.appendChild(option);
                });

                // Seleccionar el cliente actual si todavía está en la lista filtrada
                if (filteredClientes.some(cliente => cliente.id === consumo.clientId)) {
                    clientesSelect.value = consumo.clientId;
                }
                const selectedCliente = clientes.find(cliente => cliente.id === clientesSelect.value);
                consumo.cliente = selectedCliente;
                fetch(`http://localhost:9090/api/consumo/client/${consumo.id}/${clientesSelect.value}`)
                populateModal(consumo);
            };
        }

        // Función para llenar la lista de productos
        function populateProductos() {
            const productosSelect = document.getElementById("productos");
            productosSelect.innerHTML = "";

            productos.forEach(producto => {
                const option = document.createElement("option");
                option.value = producto.id;
                option.text = `${producto.nombre} - ${producto.precio}`;
                productosSelect.appendChild(option);
            });
        }
    
    // Función para rellenar el modal con datos
    function populateModal(data) {
        document.getElementById("clienteNombre").innerText = `${data.cliente.firstName} ${data.cliente.lastName}`;
        document.getElementById("clienteCedula").innerText = data.cliente.cedula;
        document.getElementById("estado").innerText = data.estado;
        document.getElementById("total").innerText = data.total;
        document.getElementById("fechaConsumo").innerText = new Date(data.fecha_consumo).toLocaleString();

        const detalleTableBody = document.getElementById("detalleTable").getElementsByTagName("tbody")[0];
        detalleTableBody.innerHTML = ""; // Limpiar la tabla antes de llenarla

        data.detalle.forEach(item => {
            const row = detalleTableBody.insertRow();
            const cellProducto = row.insertCell(0);
            const cellCategoria = row.insertCell(1);
            const cellCantidad = row.insertCell(2);
            const cellPrecio = row.insertCell(3);

            cellProducto.innerText = item.producto.nombre;
            cellCategoria.innerText = item.producto.categoriaId;
            cellCantidad.innerText = item.cantidad;
            cellPrecio.innerText = item.producto.precio;
        });
    }
    // Agregar producto al consumo
    const addProductBtn = document.getElementById("addProductBtn");
    addProductBtn.onclick = function() {
        const productosSelect = document.getElementById("productos");
        const selectedProductoId = productosSelect.value;
        const selectedProducto = productos.find(producto => producto.id === selectedProductoId);

        const existingDetalle = consumo.detalle.find(detalle => detalle.productoId === selectedProductoId);
        if (existingDetalle) {
            existingDetalle.cantidad += 1;
        } else {
            consumo.detalle.push({
                id: (consumo.detalle.length + 1).toString(),
                consumoId: consumo.id,
                productoId: selectedProductoId,
                cantidad: 1,
                producto: selectedProducto
            });
        }
        fetch(`http://localhost:9090/api/consumo/producto`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                consumoId: consumo.id,
                detalle: consumo.detalle
            })
        })
        consumo.total += selectedProducto.precio;
        populateModal(consumo);
    }
    function populateClientesCreate() {
        const clientesSelect = document.getElementById("clientesCreate");
        clientesSelect.innerHTML = "";

        clientes.forEach(cliente => {
            const option = document.createElement("option");
            option.value = cliente.id;
            option.text = `${cliente.firstName} ${cliente.lastName}`;
            clientesSelect.appendChild(option);
        });
    }
    function populateProductosCreate() {
        const productosSelect = document.getElementById("productosCreate");
        productosSelect.innerHTML = "";

        productos.forEach(producto => {
            const option = document.createElement("option");
            option.value = producto.id;
            option.text = `${producto.nombre} - ${producto.precio}`;
            productosSelect.appendChild(option);
        });
    }
     // Función para limpiar el modal de creación
     function clearCreateModal() {
        newConsumo = {
            "id": "2",
            "clientId": "",
            "estado": "abierto",
            "total": 0,
            "fecha_consumo": new Date().toISOString(),
            "fecha_cierre": null,
            "cliente": {},
            "detalle": []
        };
        document.getElementById("detalleTableCreate").getElementsByTagName("tbody")[0].innerHTML = "";
        document.getElementById("totalCreate").innerText = newConsumo.total;
    }

    // Agregar producto al nuevo consumo
    const addProductCreateBtn = document.getElementById("addProductCreateBtn");
    addProductCreateBtn.onclick = function() {
        const productosSelect = document.getElementById("productosCreate");
        const cantidadInput = document.getElementById("cantidadCreate");
        const selectedProductoId = productosSelect.value;
        const selectedProducto = productos.find(producto => producto.id === selectedProductoId);
        const cantidad = parseInt(cantidadInput.value);

        const existingDetalle = newConsumo.detalle.find(detalle => detalle.productoId === selectedProductoId);
        if (existingDetalle) {
            existingDetalle.cantidad += cantidad;
        } else {
            newConsumo.detalle.push({
                id: (newConsumo.detalle.length + 1).toString(),
                consumoId: newConsumo.id,
                productoId: selectedProductoId,
                cantidad: cantidad,
                producto: selectedProducto
            });
        }

        newConsumo.total += selectedProducto.precio * cantidad;
        populateCreateModal(newConsumo);
    }

    // Función para rellenar el modal de creación con datos
    function populateCreateModal(data) {
        const detalleTableBody = document.getElementById("detalleTableCreate").getElementsByTagName("tbody")[0];
        detalleTableBody.innerHTML = ""; // Limpiar la tabla antes de llenarla

        data.detalle.forEach(item => {
            const row = detalleTableBody.insertRow();
            const cellProducto = row.insertCell(0);
            const cellCategoria = row.insertCell(1);
            const cellCantidad = row.insertCell(2);
            const cellPrecio = row.insertCell(3);

            cellProducto.innerText = item.producto.nombre;
            cellCategoria.innerText = item.producto.categoriaId;
            cellCantidad.innerText = item.cantidad;
            cellPrecio.innerText = item.producto.precio;
        });

        document.getElementById("totalCreate").innerText = data.total;
    }

    // Guardar nuevo consumo
    const saveConsumoBtn = document.getElementById("saveConsumoBtn");
    saveConsumoBtn.onclick = function() {
        const clientesSelect = document.getElementById("clientesCreate");
        const selectedClienteId = clientesSelect.value;
        const selectedCliente = clientes.find(cliente => cliente.id === selectedClienteId);
        newConsumo.cliente = selectedCliente;
        newConsumo.clientId = selectedCliente.id;

        console.log("Nuevo consumo guardado:", newConsumo);
        const consumoData = {
            tableId: tableId,
            clientId: newConsumo.clientId,
            productos: newConsumo.detalle.map(e => {
                return {
                    id: e.productoId,
                    cantidad: e.cantidad
                }
            })
        }
        fetch('http://localhost:9090/api/consumo', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(consumoData)
        })
            .then(response => response.json())
            .then(data => {
                console.log("Consumo guardado:", data);
                cargarMesas();
            })
            .catch(error => console.error(error));
        createModal.style.display = "none";
    }

    closeModal.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        } else if (event.target == createModal) {
            createModal.style.display = "none";
        }
    }
    function cargarMesas() {
        const restaurantId = document.getElementById('restaurant').value;
        // const date = document.getElementById('date').value;
        // const timeRange = [document.getElementById('timeRange').value];
        const groupSize = document.getElementById('groupSize').value;
        if(restaurantId && groupSize) {
            fetch(`http://localhost:9090/api/consumo/mesas/${restaurantId}/${groupSize}`)
                .then(response => response.json())
                .then(data => {
                    const mesas = document.getElementById('mesas');
                    mesas.innerHTML = '';
                    data.forEach(result => {
                        const radio = document.createElement('input');
                        radio.setAttribute('type', 'radio');
                        radio.setAttribute('name', "mesa");
                        radio.setAttribute('ocupado', result.ocupado);
                        radio.setAttribute('consumoId', result.consumoId);
                        radio.value = result.id;
                        mesas.appendChild(radio);
                        const label = document.createElement('label');
                        if(result.ocupado) {
                            result.name += ' (ocupado)';
                        }
                        label.textContent = result.name;
                        mesas.appendChild(label);
                    });
                    const radios = document.querySelectorAll('input[type="radio"][name="mesa"]');

                    // Función que se ejecuta cuando se cambia la selección
                    function handleRadioChange(event) {
                        const ocupado = event.target.getAttribute("ocupado") === "true";
                        
                        const consumoId = event.target.getAttribute("consumoId");
                        const value = event.target.value;
                        console.log(`La selección ha cambiado a: Mesa ${value}`);
                        if(ocupado) {
                            const response = fetch(`http://localhost:9090/api/consumo/${consumoId}`)
                                .then(response => response.json())
                                .then(data => {
                                    console.log(data);
                                    consumo = data;
                                    modal.style.display = "block";
                                    populateModal(data);
                                    populateClientes();
                                    populateProductos();
                                });
                        }else {
                                createModal.style.display = "block";
                                tableId = value;
                                populateClientesCreate();
                                populateProductosCreate();
                                clearCreateModal();
                        }
                        // Aquí puedes agregar cualquier otra lógica que necesites

                }
                // Agregar event listener a cada radio input
                radios.forEach(radio => {
                    radio.addEventListener('change', handleRadioChange);
                });
            })
                .catch(error => console.error(error));
        }
    }
    document.getElementById('cerrarBtn').addEventListener('click',async e => {
        consumo.fecha_cierre = new Date();
        consumo.estado = 'cerrado';
        const response = await fetch(`http://localhost:9090/api/consumo/cerrar/${consumo.id}`)
        modal.style.display = "none";
        cargarMesas();
        printTicket();
    });
    document.querySelectorAll("#restaurant, #date, #timeRange, #groupSize")
        .forEach(element => element.addEventListener('change', cargarMesas));

        function printTicket() {

            let ticketWindow = window.open("", "Ticket", "width=400,height=600");
            ticketWindow.document.write("<html><head><title>Ticket</title><style>");
            ticketWindow.document.write("#ticket { font-family: Arial, sans-serif; width: 300px; margin: 0 auto; padding: 10px; border: 1px solid #000; }");
            ticketWindow.document.write("#ticket h2, #ticket h3 { text-align: center; }");
            ticketWindow.document.write("#ticket table { width: 100%; border-collapse: collapse; }");
            ticketWindow.document.write("#ticket table, #ticket th, #ticket td { border: 1px solid black; }");
            ticketWindow.document.write("#ticket th, #ticket td { padding: 5px; text-align: left; }");
            ticketWindow.document.write("</style></head><body>");
            ticketWindow.document.write("<div id='ticket'>");
            ticketWindow.document.write("<h2>Ticket de Consumo</h2>");
            ticketWindow.document.write("<h3>Cliente: " + consumo.cliente.firstName + " " + consumo.cliente.lastName + "</h3>");
            ticketWindow.document.write("<p>Cédula: " + consumo.cliente.cedula + "</p>");
            ticketWindow.document.write("<p>Fecha de Consumo: " + new Date(consumo.fecha_consumo).toLocaleString() + "</p>");
            ticketWindow.document.write("<p>Fecha de Cierre: " + new Date(consumo.fecha_cierre).toLocaleString() + "</p>");
            ticketWindow.document.write("<table><thead><tr><th>Producto</th><th>Cantidad</th><th>Precio</th></tr></thead><tbody>");

            consumo.detalle.forEach(item => {
                ticketWindow.document.write("<tr><td>" + item.producto.nombre + "</td><td>" + item.cantidad + "</td><td>" + item.producto.precio + "</td></tr>");
            });

            ticketWindow.document.write("</tbody></table>");
            ticketWindow.document.write("<p>Total: " + consumo.total + "</p>");
            ticketWindow.document.write("</div>");
            ticketWindow.document.write("</body></html>");
            ticketWindow.document.close();
        }
});
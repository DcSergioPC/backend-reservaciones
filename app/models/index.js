/*const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {

host: dbConfig.HOST,

dialect: dbConfig.dialect,

operatorsAliases: false,

pool: {

max: dbConfig.pool.max,

min: dbConfig.pool.min,

acquire: dbConfig.pool.acquire,

idle: dbConfig.pool.idle

}

});

const db = {};

db.Sequelize = Sequelize;

db.sequelize = sequelize;

db.Ventas = require("./venta.model.js")(sequelize, Sequelize);

module.exports = db;
*/

const dbConfig = require("../config/db.config.js");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.Restaurant = require("./restaurant.model.js")(sequelize, Sequelize);
db.Table = require("./table.model.js")(sequelize, Sequelize);
db.Client = require("./client.model.js")(sequelize, Sequelize);
db.Reservation = require("./reservation.model.js")(sequelize, Sequelize);
db.Categoria = require("./categoria.model.js")(sequelize, Sequelize);
db.Producto = require("./producto.model.js")(sequelize, Sequelize);
db.Consumo = require("./consumo.model.js")(sequelize, Sequelize);
db.ConsumoDetalle = require("./consumo_detalle.model.js")(sequelize, Sequelize);

// Associations
db.Restaurant.hasMany(db.Table, { foreignKey: 'restaurantId' });
db.Restaurant.hasMany(db.Reservation, { foreignKey: 'restaurantId' });
db.Table.belongsTo(db.Restaurant, { foreignKey: 'restaurantId' });
db.Table.hasMany(db.Reservation, { foreignKey: 'tableId' });
db.Client.hasMany(db.Reservation, { foreignKey: 'clientId' });
db.Reservation.belongsTo(db.Restaurant, { foreignKey: 'restaurantId' });
db.Reservation.belongsTo(db.Table, { foreignKey: 'tableId' });
db.Reservation.belongsTo(db.Client, { foreignKey: 'clientId' });
// db.Categoria.hasMany(db.Producto, { foreignKey: 'categoriaId' });
db.Producto.belongsTo(db.Categoria, { foreignKey: 'categoriaId' });
// db.Consumo.hasMany(db.ConsumoDetalle, { foreignKey: 'consumoId' });
db.ConsumoDetalle.belongsTo(db.Consumo, { foreignKey: 'consumoId' });

module.exports = db;

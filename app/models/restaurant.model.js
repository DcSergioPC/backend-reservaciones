module.exports = (sequelize, Sequelize) => {
    const Restaurant = sequelize.define("Restaurant", {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      address: {
        type: Sequelize.STRING,
        allowNull: false
      }
    });
  
    return Restaurant;
  };
  
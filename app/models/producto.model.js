module.exports = (sequelize, Sequelize) => {
    const Producto = sequelize.define("Producto", {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      nombre: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      categoriaId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        references: {
          model: 'Categoria',
          key: 'id'
        }
      },
      precio: {
        type: Sequelize.DOUBLE,
        allowNull: false
      }
    },
    {
      timestamps: false
    });
  
    return Producto;
  };
  
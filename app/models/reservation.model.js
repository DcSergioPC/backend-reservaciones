module.exports = (sequelize, Sequelize) => {
    const Reservation = sequelize.define("Reservation", {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      timeRange: {
        type: Sequelize.STRING,
        allowNull: false
      },
      partySize: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      restaurantId: {
        type: Sequelize.BIGINT,
        references: {
          model: 'Restaurants',
          key: 'id'
        }
      },
      tableId: {
        type: Sequelize.BIGINT,
        references: {
          model: 'Tables',
          key: 'id'
        }
      },
      clientId: {
        type: Sequelize.BIGINT,
        references: {
          model: 'Clients',
          key: 'id'
        }
      }
    });
  
    return Reservation;
  };
  
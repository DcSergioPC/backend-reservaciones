module.exports = (sequelize, Sequelize) => {
    const ConsumoDetalle = sequelize.define("Consumo_Detalle", {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      consumoId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        references: {
          model: 'Consumos',
          key: 'id'
        }
      },
      productoId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        references: {
          model: 'Productos',
          key: 'id'
        }
      },
      cantidad: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
      }
    },
    {
      timestamps: false
    });
  
    return ConsumoDetalle;
  };
  
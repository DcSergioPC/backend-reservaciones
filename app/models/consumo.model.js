module.exports = (sequelize, Sequelize) => {
    const Consumo = sequelize.define("Consumo", {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      tableId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        references: {
          model: 'Tables',
          key: 'id'
        }
      },
      clientId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        references: {
          model: 'Clients',
          key: 'id'
        }
      },
      estado: {
        type: Sequelize.ENUM('abierto', 'cerrado'),
        allowNull: false,
        defaultValue: 'abierto'
      },
      total: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      fecha_consumo: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      fecha_cierre: {
        type: Sequelize.DATE,
        allowNull: true
      }
    },
    {
      timestamps: false
    });
  
    return Consumo;
  };
  
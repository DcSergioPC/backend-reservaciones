module.exports = (sequelize, Sequelize) => {
    const Table = sequelize.define("Table", {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      positionX: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      positionY: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      floor: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
      },
      capacity: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      restaurantId: {
        type: Sequelize.BIGINT,
        references: {
          model: 'Restaurants',
          key: 'id'
        }
      }
    });
  
    return Table;
  };
  
const db = require("../models");
const { Op } = require("sequelize");
const Reservation = db.Reservation;
const Client = db.Client;
const Table = db.Table;

exports.create = async (req, res) => {
  // if (!req.body.restaurantId || !req.body.tableId || !req.body.date || !req.body.timeRange || !req.body.partySize || !req.body.cedula || !req.body.firstName || !req.body.lastName) {
  //   // res.status(400).send({ message: "Content cannot be empty!" });
  //   res.send({ message: "Entro hasta aca" });
  //   return;
  // }

  try {
    let client = await Client.findOne({ where: { id: req.body.clientId } });
    // if (!client) {
    //   client = await Client.create({
    //     cedula: req.body.cedula,
    //     firstName: req.body.firstName,
    //     lastName: req.body.lastName
    //   });
    // }

    const reservation = {
      restaurantId: req.body.restaurantId,
      tableId: req.body.tableId,
      date: req.body.date,
      timeRange: req.body.timeRange,
      partySize: req.body.numberOfGuests,
      clientId: client.id
    };

    const data = await Reservation.create(reservation);
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while creating the Reservation."
    });
  }
};

exports.findAll = (req, res) => {
  Reservation.findAll({
    include: [Client]
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving reservations."
      });
    });
};

exports.disponible = async (req, res) => {
  const timeRange = req.body.timeRange;
  const cantidad = req.body.partySize;
  const restaurantId = req.body.restaurantId;
  const date = req.body.date;
  const floor = req.body.floor;

  // Encontrar todas las mesas que cumplen con la capacidad requerida
  const mesas = await Table.findAll({
    where: {
      restaurantId: restaurantId,
      capacity: {
        [Op.gte]: cantidad
      },
      floor: {
        [Op.in]: floor ? floor : [1, 2]
      }
    }
  });

  // Encontrar todas las reservas para el restaurante, fecha y rango de tiempo especificados
  const reservations = await Reservation.findAll({
    where: {
      restaurantId: restaurantId,
      date: date,
      timeRange: {
        [Op.in]: timeRange
      }
    }
  });

  // Obtener los IDs de las mesas que están reservadas
  const reservedTableIds = reservations.map(reservation => reservation.tableId);

  // Filtrar las mesas que no están reservadas
  const availableTables = mesas.filter(mesa => !reservedTableIds.includes(mesa.id));

  res.send({ mesas: availableTables });
}
exports.listar = async (req, res) => {
  const clientId = req.body.clientId;
  const date = req.body.date;
  const restaurantId = req.body.restaurantId;

  const reservation = await Reservation.findAll({
    where: {
      clientId: clientId ? clientId : {
        [Op.ne]: null
      },
      date: date,
      restaurantId: restaurantId
    },
    order: [['timeRange', 'ASC'], ['tableId', 'ASC']]
  });
  res.send(reservation);
};
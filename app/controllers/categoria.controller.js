const db = require("../models");
const Categoria = db.Categoria;

exports.create = (req, res) => {
  if (!req.body.nombre) {
    res.status(400).send({ message: "Content cannot be empty!" });
    return;
  }

  const categoria = {
    nombre: req.body.nombre,
  };

  Categoria.create(categoria)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Category."
      });
    });
};

exports.findAll = (req, res) => {
  Categoria.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
        res.status(500).send({
        message: err.message || "Some error occurred while retrieving categories."
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Categoria.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Category was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Category with id=${id}. Maybe Category was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Category with id=" + id
      });
    });
}

exports.put = (req, res) => {
  const id = req.params.id;
  const body = req.body;
  Categoria.update(body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Category was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Category with id=${id}. Maybe Category was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Category with id=" + id
      });
    });
}
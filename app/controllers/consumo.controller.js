const db = require("../models");
const Client = db.Client;
const Producto = db.Producto;
const Categoria = db.Categoria;
const Op = db.Sequelize.Op;
const Table = db.Table;
const Consumo = db.Consumo;
const ConsumoDetalle = db.ConsumoDetalle;

exports.create = async (req, res) => {
  if (
      !req.body.tableId ||
      !req.body.clientId ||
      !req.body.productos ||
      !req.body.productos.length
    ) {
    res.status(400).send({ message: "Content cannot be empty!" });
    return;
  }
  try {
    
    const table = await Table.findByPk(req.body.tableId)
    const client = await Client.findByPk(req.body.clientId)
    if(!table || !client){
      res.status(400).send({ message: "No existe el cliente o la mesa!" });
      return;
    }
    const productos = req.body.productos

    let total = 0
    for(let index = 0; index < productos.length; index++) {
      const element = productos[index];
      const producto = await Producto.findByPk(element.id)
      if(!element || !parseInt(element.id) || !producto || !(parseInt(element.cantidad) > 0)){
        res.status(400).send({ message: "Los datos son incorrectos!" });
        return;
      }
      total += (element.cantidad * (producto.precio || 0))
    }
    const consumo = {
      tableId: table.id,
      clientId: client.id,
      total: total
    }
    const resultConsumo = (await Consumo.create(consumo)).toJSON();
    resultConsumo.cliente = (await Client.findByPk(client.id));
    let resultDetalle = []
    for (let index = 0; index < productos.length; index++) {
      const element = productos[index];
      const detalle = {
        consumoId: resultConsumo.id,
        productoId: element.id,
        cantidad: element.cantidad
      }
      const consumoDetalle = (await ConsumoDetalle.create(detalle)).toJSON();
      consumoDetalle.producto = (await Producto.findByPk(element.id));
      resultDetalle.push(consumoDetalle);
    };
    res.send({
      consumo: resultConsumo,
      detalle: resultDetalle
    });
  } catch (err) {
    res.status(500).send({ message: err.message || "Some error occurred while creating the Product." });
  }
};


exports.findAll = async (req, res) => {
  const id = req.params.id;
  if(id){
    const data = await Consumo.findByPk(id)
    const consumo = data?.toJSON();
    consumo.cliente = await Client.findByPk(data.clientId);
    consumo.detalle = await ConsumoDetalle.findAll({where: {consumoId: data.id}});
    consumo.detalle = consumo.detalle.map((item) => {
      return item.toJSON();
    })
    for (let index = 0; index < consumo.detalle.length; index++) {
      const element = consumo.detalle[index];
      const producto = await Producto.findByPk(element.productoId)
      element.producto = producto.toJSON();
    }
    res.send(consumo);
    return;
  }
  Consumo.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
        res.status(500).send({
        message: err.message || "Some error occurred while retrieving consumo."
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Consumo.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Consumo was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Consumo with id=${id}. Maybe Consumo was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Consumo with id=" + id
      });
    });
};

exports.cerrar = async (req, res) => {
  const id = req.params.id;

  try {
    const [num] = await Consumo.update({
      estado: "cerrado",
      fecha_cierre: new Date() // Asegúrate de tener este campo en el modelo si lo usas
    }, {
      where: { id: id }
    });

    if (num === 1) {
      res.send({
        message: "Consumo was updated successfully."
      });
    } else {
      res.send({
        message: `Cannot update Consumo with id=${id}. Maybe Consumo was not found or req.body is empty!`
      });
    }
  } catch (err) {
    res.status(500).send({
      message: "Error updating Consumo with id=" + id
    });
  }
};

exports.traerMesas = async (req, res) => {
  const restaurantId = req.params.restaurantId;
  const groupSize = req.params.groupSize;
  
  const mesas = await Table.findAll({
    where: {
      restaurantId: restaurantId,
      capacity: {
        [Op.gte]: groupSize
      }
    }
  });

  const consumos = await Consumo.findAll({
    where: {
      estado: "abierto",
      tableId: {
        [Op.in]: mesas.map((mesa) => mesa.id)
      }
    }
  });
  const result = mesas.map((mesa) => {
    const c = consumos.find((consumo) => consumo.tableId === mesa.id)
    if(c){
      return {
        ...mesa.toJSON(),
        ocupado: true,
        consumoId: c.id
      };
    }
    return {
      ...mesa.toJSON(),
      ocupado: false
    };
  });
  
  res.send(result);
}

exports.clientUpdate = async (req, res) => {
  const consumoId = req.params.consumoId;
  const clientId = req.params.clientId;
  const [num] = await Consumo.update({
    clientId: clientId
  }, {
    where: { id: consumoId }
  });
  if (num == 1) {
    res.send({
      message: "Consumo was updated successfully."
    });
  } else {
    res.send({
      message: `Cannot update Consumo with id=${id}. Maybe Consumo was not found or req.body is empty!`
    });
  }
}

exports.producto = async (req, res) => {
  const consumoId = req.body.consumoId;
  const detalle = req.body.detalle;

  const result = await ConsumoDetalle.findAll({
    where: {
      consumoId: consumoId
    }
  });

  // Crear un mapa de productoId a los detalles de consumo existentes
  const existingDetailsMap = new Map();
  result.forEach(element => {
    existingDetailsMap.set(element.productoId, element);
  });

  // Procesar cada detalle del consumo recibido
  for (let i = 0; i < detalle.length; i++) {
    const item = detalle[i];
    const existingDetail = existingDetailsMap.get(item.productoId);

    if (existingDetail) {
      // Si el detalle ya existe, actualizar la cantidad
      await ConsumoDetalle.update({
        cantidad: item.cantidad
      }, {
        where: {
          consumoId: consumoId,
          productoId: item.productoId
        }
      });
    } else {
      // Si el detalle no existe, crear un nuevo registro
      await ConsumoDetalle.create({
        consumoId: consumoId,
        productoId: item.productoId,
        cantidad: item.cantidad
      });
    }
  }

  // Obtener y enviar la lista actualizada de detalles de consumo
  const updatedResult = await ConsumoDetalle.findAll({
    where: {
      consumoId: consumoId
    }
  });
  //calcular precio total
  let total = 0
  for (let i = 0; i < updatedResult.length; i++) {
    const item = updatedResult[i];
    const producto = await Producto.findByPk(item.productoId)
    item.producto = producto.toJSON();
    total += (item.cantidad * (item.producto.precio || 0))
  }
  await Consumo.update({
    total: total
  }, {
    where: { id: consumoId }
  });

  res.send(updatedResult);
};
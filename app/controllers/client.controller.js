const db = require("../models");
const Client = db.Client;

exports.create = (req, res) => {
  if (!req.body.cedula || !req.body.firstName || !req.body.lastName) {
    res.status(400).send({ message: "Content cannot be empty!" });
    return;
  }

  const client = {
    cedula: req.body.cedula,
    firstName: req.body.firstName,
    lastName: req.body.lastName
  };

  Client.create(client)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Client."
      });
    });
};

exports.findAll = (req, res) => {
  Client.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
        res.status(500).send({
        message: err.message || "Some error occurred while retrieving clients."
      });
    });
};

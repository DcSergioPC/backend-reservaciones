const db = require("../models");
const ConsumoDetalle = db.ConsumoDetalle;
const Producto = db.Producto;

exports.create = (consumoId,productoId,cantidad) => {
  const consumo_detalle = {
    consumoId,
    productoId,
    cantidad
  };

  ConsumoDetalle.create(consumo_detalle)
};

exports.findAll = (req, res) => {
  const id = req.params.id;
  if(id){
    Producto.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Product with id=" + id
      });
    });
    return;
  }
  ConsumoDetalle.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
        res.status(500).send({
        message: err.message || "Some error occurred while retrieving productos."
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Producto.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Product was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Product with id=${id}. Maybe Product was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Product with id=" + id
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Producto.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Product was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Product with id=${id}. Maybe Product was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Product with id=" + id
      });
    });
};

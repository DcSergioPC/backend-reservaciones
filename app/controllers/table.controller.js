const db = require("../models");
const Op = db.Sequelize.Op;
const Table = db.Table;
const Reservation = db.Reservation;
const Restaurant = db.Restaurant;

exports.create = (req, res) => {
  if (!req.body.name || !req.body.positionX || !req.body.positionY || !req.body.capacity || !req.body.restaurantId) {
    res.status(400).send({ message: "Content cannot be empty!" });
    return;
  }

  const table = {
    name: req.body.name,
    positionX: req.body.positionX,
    positionY: req.body.positionY,
    floor: req.body.floor || 1,
    capacity: req.body.capacity,
    restaurantId: req.body.restaurantId
  };

  Table.create(table)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Table."
      });
    });
};

exports.findAll = (req, res) => {
  Table.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving tables."
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Table.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find Table with id=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Table with id=" + id
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Table.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Table was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Table with id=${id}. Maybe Table was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Table with id=" + id
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Table.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Table was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Table with id=${id}. Maybe Table was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Table with id=" + id
      });
    });
};


exports.mesas = async (req, res) => {
  const restaurantId = req.body.restaurantId;
  const date = req.body.date;
  const timeRange = req.body.timeRange;
  const groupSize = req.body.groupSize;
  const floor = req.body.floor;
    const mesas = await Table.findAll({
      where: {
        restaurantId: restaurantId,
        // capacity: {
        //   [Op.gte]: groupSize
        // },
        floor: {
          [Op.in]: floor ? floor : [1, 2]
        }
      }
    });
  
    // Encontrar todas las reservas para el restaurante, fecha y rango de tiempo especificados
    const reservations = await Reservation.findAll({
      where: {
        restaurantId: restaurantId,
        date: date,
        timeRange: {
          [Op.in]: timeRange
        }
      }
    });
  
    // Obtener los IDs de las mesas que están reservadas
    const reservedTableIds = reservations.map(reservation => reservation.tableId);
  
    // Filtrar las mesas que no están reservadas
    const availableTables = mesas.map(mesa => {
      if (reservedTableIds.includes(mesa.id)) {
        return {
          mesa: {
            id: mesa.id,
            name: mesa.name
          },
          ocupado: true
        };
      } else {
        return {
          mesa: {
            id: mesa.id,
            name: mesa.name
          },
          ocupado: false
        };
      }
    });
  
    res.send(availableTables);
}
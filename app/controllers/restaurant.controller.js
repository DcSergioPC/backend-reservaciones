const db = require("../models");
const Restaurant = db.Restaurant;

exports.create = (req, res) => {
  // if (!req.body.name || !req.body.address) {
  //   res.status(400).send({ message: "Content cannot be empty!" });
  //   return;
  // }

  const restaurant = {
    name: req.body.name,
    address: req.body.address
  };

  Restaurant.create(restaurant)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Restaurant."
      });
    });
};

exports.findAll = (req, res) => {
  Restaurant.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving restaurants."
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Restaurant.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find Restaurant with id=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Restaurant with id=" + id
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Restaurant.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Restaurant was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Restaurant with id=${id}. Maybe Restaurant was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Restaurant with id=" + id
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Restaurant.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Restaurant was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Restaurant with id=${id}. Maybe Restaurant was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Restaurant with id=" + id
      });
    });
};

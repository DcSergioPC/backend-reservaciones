const db = require("../models");
const Producto = db.Producto;
const Categoria = db.Categoria;

exports.create = (req, res) => {
  if (!req.body.nombre || !req.body.categoriaId || !req.body.precio) {
    res.status(400).send({ message: "Content cannot be empty!" });
    return;
  }

  const producto = {
    nombre: req.body.nombre,
    categoriaId: req.body.categoriaId,
    precio: req.body.precio
  };

  Producto.create(producto)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the producto."
      });
    });
};

exports.findAll = (req, res) => {
  const id = req.params.id;
  if(id){
    Producto.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Product with id=" + id
      });
    });
    return;
  }
  Producto.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
        res.status(500).send({
        message: err.message || "Some error occurred while retrieving productos."
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Producto.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Product was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Product with id=${id}. Maybe Product was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Product with id=" + id
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Producto.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Product was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Product with id=${id}. Maybe Product was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Product with id=" + id
      });
    });
};

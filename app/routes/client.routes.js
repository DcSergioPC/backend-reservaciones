/*module.exports = app => {
    const client = require('../controllers/client.controller');
    const router = require('express').Router();

    // Definición de rutas CRUD para clientes
    router.post('/', client.create);
    router.get('/', client.findAll);

    app.use('/api/clients', router);  // Montar las rutas en 'app'
};
*/
// client.routes.js
const express = require('express');
const router = express.Router();
const clientController = require('../controllers/client.controller');

// Rutas CRUD para clientes
router.post('/', clientController.create);
router.get('/', clientController.findAll);

module.exports = router;  // Asegúrate de exportar el router

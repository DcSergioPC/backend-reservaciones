
// client.routes.js
const express = require('express');
const router = express.Router();
const categoriaController = require('../controllers/categoria.controller');

// Rutas CRUD para clientes
router.post('/', categoriaController.create);
router.get('/', categoriaController.findAll);

module.exports = router;  // Asegúrate de exportar el router

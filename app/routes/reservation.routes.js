
const express = require('express');
const router = express.Router();
const reservation = require('../controllers/reservation.controller');

// Rutas CRUD para clientes
router.post('/', reservation.create);
router.get('/', reservation.findAll);
router.post('/disponible', reservation.disponible);
router.post('/listar', reservation.listar);

module.exports = router; 
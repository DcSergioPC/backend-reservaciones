
  const express = require('express');
const router = express.Router();
const tableController = require('../controllers/table.controller');

// Ruta para crear una mesa
router.post('/', tableController.create);
router.post('/mesas', tableController.mesas);

// Otras rutas como GET, PUT, DELETE según sea necesario
router.get('/', tableController.findAll);
router.get('/:id', tableController.findOne);
router.put('/:id', tableController.update);
router.delete('/:id', tableController.delete);

module.exports = router;

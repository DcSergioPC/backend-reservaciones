
// client.routes.js
const express = require('express');
const router = express.Router();
const productoController = require('../controllers/producto.controller');

// Rutas CRUD para clientes
router.post('/', productoController.create);
router.get('/', productoController.findAll);

module.exports = router;  // Asegúrate de exportar el router

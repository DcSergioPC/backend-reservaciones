
// client.routes.js
const express = require('express');
const router = express.Router();
const consumoController = require('../controllers/consumo.controller');

// Rutas CRUD para clientes
router.post('/', consumoController.create);
router.get('/', consumoController.findAll);
router.get('/:id', consumoController.findAll);
router.get('/cerrar/:id', consumoController.cerrar);
router.get('/mesas/:restaurantId/:groupSize', consumoController.traerMesas);
router.get('/client/:consumoId/:clientId', consumoController.clientUpdate);
router.post('/producto/', consumoController.producto);

module.exports = router;  // Asegúrate de exportar el router
